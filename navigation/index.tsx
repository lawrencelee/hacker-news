import React from "react";
import {
	Image,
	TouchableOpacity,
	StyleSheet,
	View,
	Pressable
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AntDesign } from '@expo/vector-icons';

import HomePage from "../app/Screens/HomePage";
import Comment from "../app/Screens/Comment";

const Stack = createStackNavigator();

function App() {
	const BackButton = (props) => (
		<Pressable
	    onPress={() => props.goBack()}
	    style={({ pressed }) => ({
	      opacity: pressed ? 0.5 : 1,
	    })}
	  >
			<AntDesign name="left" size={24} color="black" />
		</Pressable>
	);
	
  return (
		<NavigationContainer>
			<Stack.Navigator initialRouteName="HomePage" screenOptions={{headerTitleAlign: 'center', gesturesEnabled: false}}>
				<Stack.Screen
					name="HomePage"
					component={HomePage}
					options={({ navigation, route }) => ({
						title: 'Hacker News',
						headerStyle: styles.headerStyle,
						headerTitleStyle: styles.headerTitleStyle,
					})}
				/>
				<Stack.Screen
					name="Comment"
					component={Comment}
					options={({ navigation, route }) => ({
						title: 'Comments',
						headerStyle: styles.headerStyle,
						headerTitleStyle: styles.headerTitleStyle,
						headerLeft: props => <BackButton {...navigation}/>
					})}
				/>
			</Stack.Navigator>
		</NavigationContainer>
  );
}

const styles = StyleSheet.create({
	headerStyle: {
		backgroundColor: '#ff6600',
	},
  headerTitleStyle: {
		fontSize: 20,
		color: 'black',
		fontWeight: 'bold',
		textAlign: 'center'
  },
});

export default App
