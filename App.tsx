import React, { useState, useEffect } from "react";
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Route from './navigation';

export default function App() {

  return (
    <SafeAreaProvider>
      <Route />
      <StatusBar style={'light'}/>
    </SafeAreaProvider>
  );
}
