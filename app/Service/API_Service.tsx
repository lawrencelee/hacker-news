import axios from 'axios';
import { BASE_API_URL } from './Environment';

const getStory = async (id) => {
  try {
    const story = await axios.get(`${BASE_API_URL}/item/${id}.json?printpretty`);
    return story.data;
  } catch (error) {
  }
};

export const getStories = async () => {
  try {
    const { data: storyIds } = await axios.get(
      `${BASE_API_URL}/topstories.json`
    );
    const stories = await Promise.all(storyIds.slice(0, 30).map(getStory));
    return stories;
  } catch (error) {
    return [];
  }
};

export const getComment = async (ids) => {
  try {
    const stories = await Promise.all(ids.map(getStory));
    return stories;
  } catch (error) {
    return [];
  }
};
