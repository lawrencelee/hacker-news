
export const getTime = (postDate) => {

  var timeSince = (Date.now() / 1000) - postDate;
  var days = Math.floor(timeSince / (60 * 60 * 24));

  if (days)
     return days + " days ago";

  var hours = Math.floor(timeSince / (60 * 60));

  if (hours)
     return hours + " hours ago";

  var minutes = Math.floor(timeSince / 60);

  return minutes + " minutes ago";
}
