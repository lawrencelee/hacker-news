import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { AntDesign } from '@expo/vector-icons';

import { getComment } from '../../Service/API_Service';
import { getTime } from "../../Component/getTime";

function App(props) {

  const { navigation, route } = props;
  const [stories, setStories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getComment(route.params.ids)
      .then((stories) => {
        setStories(stories);
        setIsLoading(false);
      })
      .catch(() => {
        alert('Unable to retrieve data. Please try again later');
        setIsLoading(false);
      });
  }, []);

  const renderItem=(item, index)=>{
    return (
      <TouchableOpacity
        style={{margin: 10}}
        onPress={() => navigation.push('Comment', {ids: item.kids, title: item.text})}
      >
        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 5}}>
          <AntDesign name="caretright" size={15} color="grey" />
          <View style={{flexDirection: 'row', marginLeft: 5}}>
            <Text>{item.by} {getTime(item.time)} | {item.kids && item.kids.length > 0 ? item.kids.length : 0} comments</Text>
          </View>
        </View>
        <Text style={{fontSize: 15, fontWeight: 'bold'}}>
          {item.text}
        </Text>
      </TouchableOpacity>
    );
  }

  if (isLoading) {
    return(
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: 'center',
          backgroundColor: '#00000002'
        }}
      >
        <ActivityIndicator size="large" color={"black"} />
      </View>
    )
  }

  return(
    <SafeAreaView style={{flex: 1}}>
      <Text style={{fontSize: 15, fontWeight: 'bold', margin: 10}} numberOfLines={3}>
        {route.params.title}
      </Text>
      <FlatList
        style={{marginHorizontal: 10}}
        data={stories}
        bounces={false}
        keyExtractor={(item,index) => index.toString()}
        renderItem={({ item, index }) => renderItem(item, index)}
        ItemSeparatorComponent={() => <View style={{height: 5}}/>}
        ListEmptyComponent={()=> (
          <Text style={{textAlign: 'center',padding:15}}>Empty comment</Text>
        )}
      />
    </SafeAreaView>
  );
}

export default App
