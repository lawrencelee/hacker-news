import React, { useState, useEffect  } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from 'react-native';

import { getStories } from '../../Service/API_Service';
import { getTime } from "../../Component/getTime";

function App(props) {

  const { navigation } = props;
  const [stories, setStories] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getStories()
      .then((stories) => {
        setStories(stories);
        setIsLoading(false);
      })
      .catch(() => {
        alert('Unable to retrieve data. Please try again later');
        setIsLoading(false);
      });
  }, []);

  const renderItem=(item, index)=>{
    return (
      <TouchableOpacity
        style={{margin: 10}}
        onPress={() => navigation.navigate('Comment', {ids: item.kids, title: item.title})}
      >
        <Text style={{fontSize: 15, fontWeight: 'bold'}}>
          {index+1}. {item.title}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Text>{item.score} points by {item.by} {getTime(item.time)} | {item.kids && item.kids.length > 0 ? item.kids.length : 0} comments</Text>
        </View>
      </TouchableOpacity>
    );
  }

  if (isLoading) {
    return(
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: 'center',
          backgroundColor: '#00000002'
        }}
      >
        <ActivityIndicator size="large" color={"black"} />
      </View>
    )
  }

  return(
    <SafeAreaView style={{flex: 1}}>
      <FlatList
        data={stories}
        bounces={false}
        keyExtractor={(item,index) => index.toString()}
        renderItem={({ item, index }) => renderItem(item, index)}
        ItemSeparatorComponent={() => <View style={{height: 5}}/>}
        ListEmptyComponent={()=> (
          <Text style={{textAlign: 'center',padding:15}}>Empty comment</Text>
        )}
      />
    </SafeAreaView>
  );
}

export default App
